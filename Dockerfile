FROM rust:1-slim
ARG MDBOOK_VERSION="0.4.1"
LABEL maintainer="myl0gcontact@gmail.com" \
      version=$MDBOOK_VERSION

RUN cargo install mdbook --vers ${MDBOOK_VERSION}

WORKDIR /data
VOLUME ["/data"]
