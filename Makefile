all: build

build:
	@docker build -t myl0g/mdbook:latest .

release: build
	@docker build -t myl0g/mdbook:$(shell cat Dockerfile | \
		grep "ARG MDBOOK_VERSION" | \
		sed -e 's/[^"]*"\([^"]*\)".*/\1/') .
